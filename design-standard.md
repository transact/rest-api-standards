This document has moved.

Go to [https://transactsp.atlassian.net/wiki/spaces/SPSS/pages/397770835/API+Design+Standard](https://transactsp.atlassian.net/wiki/spaces/SPSS/pages/397770835/API+Design+Standard) for the living document.

Contact ryan.haber@blackboard.com for access if you don't have it.