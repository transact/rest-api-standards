# README #

This repo contains Blackboard's REST API Standards documents and supporting resources.

---

**[design-standard.md](https://bitbucket.org/transact/rest-api-standards/src/master/design-standard.md)** details the standard for API design.

Status: draft under review

---

**[application-registration.md](https://bitbucket.org/transact/rest-api-standards/src/master/application-registration.md)** details the process by which applications are registered and controlled with Bb's API gateway.

Status: in development

---