# REST Application Registration

<!-- Note on TOC:
	Do not directly modify the TOC or heading anchors below. Ryan Haber uses a tool to autogenerate it.

	The internal links work in real life - it's only Bitbucket Cloud that doesn't respect them. Bitbucket Server and the rest of the world do.

	https://community.atlassian.com/t5/Bitbucket-questions/Markdown-internal-links-support-in-Bitbucket-Server/qaq-p/595033
	https://bitbucket.org/site/master/issues/8276/support-internal-anchor-links-in-rendered
-->

<!-- MarkdownTOC -->

- [Introduction to the Standard](#introduction-to-the-standard)
- [Overarching Design Principles](#overarching-design-principles)
	- [Requirement: Customer-centered.](#requirement-customer-centered)
	- [Requirement: Consistent developer experience](#requirement-consistent-developer-experience)
	- [Requirement: Clear usage](#requirement-clear-usage)
	- [Requirement: Correct and complete documentation](#requirement-correct-and-complete-documentation)
- [Resource Naming Conventions](#resource-naming-conventions)

<!-- /MarkdownTOC -->


<a id="introduction-to-the-standard"></a>
## Introduction to the Standard

XYZ

<a id="overarching-design-principles"></a>
## Overarching Design Principles

XYZ

<a id="requirement-customer-centered"></a>
### Requirement: Customer-centered.

XYZ
```

<a id="requirement-consistent-developer-experience"></a>
### Requirement: Consistent developer experience

XYZ

<a id="requirement-clear-usage"></a>
### Requirement: Clear usage

XYZ

<a id="requirement-correct-and-complete-documentation"></a>
### Requirement: Correct and complete documentation

XYZ

<a id="resource-naming-conventions"></a>
## Resource Naming Conventions

XYZ